import { Auth0UserConnection } from "./Auth0UserConnection";
import { Metadata } from "./Metadata";

export interface Auth0User {
  client_id: string;
  created_at: string;
  identities: Auth0UserConnection[];
  login: string;
  name: string;
  nickname: string;
  picture: string;
  scopes: string[];
  twitchAccessToken: string;
  updated_at: string;
  user_id: string;
  email: string;
  email_verified: boolean;
  last_ip: string;
  last_login: string;
  logins_count: number;
  app_metadata: Metadata;
  user_metadata: Metadata;
}
