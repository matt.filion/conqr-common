export interface Auth0UserConnection {
  provider: string;
  access_token: string;
  refresh_token: string;
  user_id: string;
  expires_in: number;
  connection: string;
  isSocial: true;
}