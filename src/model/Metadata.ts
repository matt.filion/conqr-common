export class Metadata { 
  [key: string]: string | Date | number | boolean | Metadata
}