import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { Logger, LoggerService } from '@mu-ts/logger';
import { Auth0User } from '../model/Auth0User';
import { Auth0UserConnection } from '../model/Auth0UserConnection';
import { Metadata } from 'src/model/Metadata';

export class Auth0ManagementService {
  private readonly auth0API: AxiosInstance;
  private readonly logger: Logger;
  private accessToken: string;

  constructor() {
    this.logger = LoggerService.named(this.constructor.name);
    this.auth0API = axios.create({
      baseURL: `https://conqr.us.auth0.com`,
      timeout: 120000,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  /**
   * 
   * @returns 
   */
  private async getAccessToken(): Promise<string> {
    this.logger.debug('getAccessToken()', 'exists already', { exists: !!this.accessToken });
    if (!this.accessToken) {
      this.logger.debug('getAccessToken()', 'Sending request to get a new token.');

      const response: AxiosResponse<{ access_token: string; token_type: string }> = await this.auth0API.post<{ access_token: string; token_type: string }>(
        'oauth/token',
        {
          client_id: process.env.AUTH0_CLIENT_ID,
          client_secret: process.env.AUTH0_CLIENT_SECRET,
          audience: process.env.AUTH0_HOST,
          grant_type: 'client_credentials'
        }
      );

      this.logger.debug('getAccessToken()', 'token created and set on instance');
      this.accessToken = response.data.access_token;
    }

    this.logger.debug('getAccessToken()', 'returning token');
    return this.accessToken;
  }

  /**
   *
   * @param userId
   * @returns
   */
  public async getUser(userId: string): Promise<Auth0User> {
    this.logger.debug('getUser()', 'Load a user to get the auth details for the user.');

    const accessToken: string = await this.getAccessToken();
    const response: AxiosResponse<Auth0User> = await this.auth0API.get<Auth0User>(`/api/v2/users/${userId}`, {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    });

    this.logger.debug('getUser()', 'Response received.', { data: response.data });
    return response.data;
  }

  /**
   * update:users_app_metadata
   *
   * @param userId
   * @returns
   */
   public async updateMetadata(userId: string, app_metadata?: Metadata, user_metadata?: Metadata): Promise<Auth0User> {
    this.logger.debug('updateMetadata()', 'Update the users app or user metadata, at least one of either needs to be provided or this will no-op.');

    if(!app_metadata && !user_metadata) throw new Error("Must provide at least one of user_metadat or app_metadata.")

    const accessToken: string = await this.getAccessToken();
    const response: AxiosResponse<Auth0User> = await this.auth0API.patch<Auth0User>(
      `/api/v2/users/${userId}`,
      { app_metadata, user_metadata },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }
    );

    this.logger.debug('updateMetadata()', 'Response received.', { data: response.data });
    return response.data;
  }

  /**
   * Get
   *
   * @param user
   * @param connectionName
   * @returns
   */
  public getConnectionAccessToken(user: Auth0User, connectionName: string) {
    this.logger.debug('getConnectionAccessToken()', 'get access token for connection', { user, connectionName });
    const auth0UserConnection: Auth0UserConnection | undefined = user.identities.find(
      (connection: Auth0UserConnection) => connection.connection === connectionName
    );
    this.logger.debug('getConnectionAccessToken()', 'found connection, getting access token.', { auth0UserConnection });
    if (!auth0UserConnection) throw new Error(`There is no ${connectionName} identity configured on the user.`);
    return auth0UserConnection.access_token;
  }
}
process.env.LOG_LEVEL = 'DEBUG';


process.env.AUTH0_CLIENT_ID = 'lS1t2rVD5AQwoRUee6nr0ysQWwma9SuN';
process.env.AUTH0_CLIENT_SECRET = 'dgvePpWzUzhCBjCU3vS74VoV3YUcI9fTf96SZFBQd5EDB-6N0OmAzdDvzj8ppxOh';
process.env.AUTH0_HOST = 'https://conqr.us.auth0.com/api/v2/';

new Auth0ManagementService().getUser('oauth2|twitch|185166102').then( (user) => {
  console.log(new Auth0ManagementService().getConnectionAccessToken(user,'twitch'));
})